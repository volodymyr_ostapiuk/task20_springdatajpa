package com.ostapiuk.Repository;

import com.ostapiuk.domain.PlaneType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlaneTypeRepository extends JpaRepository<PlaneType, Long> {

}
