package com.ostapiuk.service;

import com.ostapiuk.Repository.PlaneRepository;
import com.ostapiuk.Repository.PlaneTypeRepository;
import com.ostapiuk.domain.Plane;
import com.ostapiuk.domain.PlaneType;
import com.ostapiuk.exceptions.NoSuchPlaneException;
import com.ostapiuk.exceptions.NoSuchPlaneTypeException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class BookService {

    @Autowired
    PlaneRepository planeRepository;

    @Autowired
    PlaneTypeRepository planeTypeRepository;

    public List<Plane> getPlaneByPlaneTypeID(Long laptopID) throws NoSuchPlaneTypeException {
        PlaneType planeType = planeTypeRepository.findById(laptopID).get();
        if (planeType == null) {
            throw new NoSuchPlaneTypeException();
        }
        return planeType.getPlanes();
    }

    public Plane getPlane(Long accountID) throws NoSuchPlaneException {
        Plane plane = planeRepository.findById(accountID).get();
        if (plane == null) {
            throw new NoSuchPlaneException();
        }
        return plane;
    }

    public List<Plane> getAllPlanes() {
        return planeRepository.findAll();
    }

    @Transactional
    public void createPlane(Plane plane, Long planeID) throws NoSuchPlaneTypeException {
        if (planeID > 0) {
            PlaneType planeType = planeTypeRepository.findById(planeID).get();
            if (planeType == null) {
                throw new NoSuchPlaneTypeException();
            }
            plane.setPlaneTypeById(planeType);
        }
        planeRepository.save(plane);
    }

    @Transactional
    public void updatePlane(Plane updatedPlane, Long planeID, Long planeTypeID)
            throws NoSuchPlaneTypeException, NoSuchPlaneException {
        PlaneType planeType = planeTypeRepository.findById(planeTypeID).get();
        if (planeTypeID > 0) {
            if (planeType == null) {
                throw new NoSuchPlaneTypeException();
            }
        }
        Plane plane = planeRepository.findById(planeID).get();
        if (plane == null) {
            throw new NoSuchPlaneException();
        }
        plane.setName(updatedPlane.getName());
        plane.setAmount(updatedPlane.getAmount());
        if (planeTypeID > 0) {
            plane.setPlaneTypeById(planeType);
        } else {
            plane.setPlaneTypeById(null);
        }
        planeRepository.save(plane);
    }

    @Transactional
    public void deletePlane(Long planeID) throws NoSuchPlaneException {
        Plane plane = planeRepository.findById(planeID).get();
        if (plane == null) {
            throw new NoSuchPlaneException();
        }
        planeRepository.delete(plane);
    }
}
