package com.ostapiuk.DTO;

import com.ostapiuk.controller.BookController;
import com.ostapiuk.domain.Plane;
import com.ostapiuk.exceptions.NoSuchClientException;
import com.ostapiuk.exceptions.NoSuchPlaneException;
import com.ostapiuk.exceptions.NoSuchPlaneTypeException;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceSupport;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

public class PlaneDTO extends ResourceSupport {
    Plane plane;

    public PlaneDTO(Plane plane, Link selfLink)
            throws NoSuchPlaneException, NoSuchPlaneTypeException {
        this.plane = plane;
        add(selfLink);
        add(linkTo(methodOn(BookController.class).getPlaneByPlaneTypeID(plane.getId()))
                .withRel("customers"));
    }

    public Long getPlaneId() {
        return plane.getId();
    }

    public String getPlaneName() {
        return plane.getName();
    }

    public Integer getPlaneAmount() {
        return plane.getAmount();
    }

    public String getPlaneTypeById() {
        if (plane.getPlaneTypeById() == null) {
            return "";
        }
        return plane.getPlaneTypeById().getName();
    }
}
