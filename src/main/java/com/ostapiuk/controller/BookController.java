package com.ostapiuk.controller;

import com.ostapiuk.DTO.PlaneDTO;
import com.ostapiuk.domain.Plane;
import com.ostapiuk.exceptions.NoSuchPlaneException;
import com.ostapiuk.exceptions.NoSuchPlaneTypeException;
import com.ostapiuk.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
public class BookController {

    @Autowired
    BookService bookService;

    @GetMapping(value = "/api/plane/plane_type/{id}")
    public ResponseEntity<List<PlaneDTO>> getPlaneByPlaneTypeID(@PathVariable Long planeTypeID)
            throws NoSuchPlaneTypeException, NoSuchPlaneException {
        List<Plane> planeList = bookService.getPlaneByPlaneTypeID(planeTypeID);

        Link link = linkTo(methodOn(BookController.class).getAllAccounts()).withSelfRel();

        List<PlaneDTO> accountsDTO = new ArrayList<>();
        for (Plane entity : planeList) {
            Link selfLink = new Link(link.getHref() + "/" + entity.getId()).withSelfRel();
            PlaneDTO dto = new PlaneDTO(entity, selfLink);
            accountsDTO.add(dto);
        }
        return new ResponseEntity<>(accountsDTO, HttpStatus.OK);
    }

    @GetMapping(value = "/api/plane/{id}")
    public ResponseEntity<PlaneDTO> getPlane(@PathVariable Long planeID)
            throws NoSuchPlaneTypeException, NoSuchPlaneException {
        Plane plane = bookService.getPlane(planeID);
        Link link = linkTo(methodOn(BookController.class).getPlane(planeID)).withSelfRel();

        PlaneDTO planeDTO = new PlaneDTO(plane, link);

        return new ResponseEntity<>(planeDTO, HttpStatus.OK);
    }

    @GetMapping(value = "/api/plane")
    private ResponseEntity<List<PlaneDTO>> getAllAccounts()
            throws NoSuchPlaneTypeException, NoSuchPlaneException {
        List<Plane> planeList = bookService.getAllPlanes();
        Link link = linkTo(methodOn(BookController.class).getAllAccounts()).withSelfRel();

        List<PlaneDTO> accountsDTO = new ArrayList<>();
        for (Plane entity : planeList) {
            Link selfLink = new Link(link.getHref() + "/" + entity.getId()).withSelfRel();
            PlaneDTO dto = new PlaneDTO(entity, selfLink);
            accountsDTO.add(dto);
        }
        return new ResponseEntity<>(accountsDTO, HttpStatus.OK);
    }

    @PostMapping(value = "/api/plane/plane_type/{id}")
    public ResponseEntity<PlaneDTO> addClient(@RequestBody Plane newPlane, @PathVariable Long accountID)
            throws NoSuchPlaneTypeException, NoSuchPlaneException {
        bookService.createPlane(newPlane, accountID);
        Link link = linkTo(methodOn(BookController.class).getPlane(newPlane.getId())).withSelfRel();

        PlaneDTO planeDTO = new PlaneDTO(newPlane, link);

        return new ResponseEntity<>(planeDTO, HttpStatus.CREATED);
    }

    @PutMapping(value = "/api/plane/{id}/plane_type/{id}")
    public ResponseEntity<PlaneDTO> updateClient(@RequestBody Plane uPlane,
                                                 @PathVariable Long planeID, @PathVariable Long plane_typeID)
            throws NoSuchPlaneTypeException, NoSuchPlaneException {
        bookService.updatePlane(uPlane, planeID, plane_typeID);
        Plane plane = bookService.getPlane(planeID);
        Link link = linkTo(methodOn(BookController.class).getPlane(planeID)).withSelfRel();

        PlaneDTO planeDTO = new PlaneDTO(plane, link);

        return new ResponseEntity<>(planeDTO, HttpStatus.OK);
    }

    @DeleteMapping(value = "/api/plane/{id}")
    public ResponseEntity deleteClient(@PathVariable Long accountID) throws NoSuchPlaneException {
        bookService.deletePlane(accountID);
        return new ResponseEntity(HttpStatus.OK);
    }
}
