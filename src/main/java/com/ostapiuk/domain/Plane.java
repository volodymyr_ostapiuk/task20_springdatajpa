package com.ostapiuk.domain;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "plane", schema = "airport")
public class Plane {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    @Basic
    @Column(name = "name", nullable = false, length = 45)
    private String name;


    @Basic
    @Column(name = "amount", nullable = false, length = 45)
    private Integer amount;

    @ManyToOne
    @JoinColumn(name = "plane_type_id", referencedColumnName = "id", nullable = false)
    private PlaneType planeTypeById;

    public Plane() {
    }

    public Plane(String name) {
        this.name = name;
    }

    public Plane(String name, Integer amount) {
        this.name = name;
        this.amount = amount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public PlaneType getPlaneTypeById() {
        return planeTypeById;
    }

    public void setPlaneTypeById(PlaneType planeTypeById) {
        this.planeTypeById = planeTypeById;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Plane that = (Plane) o;

        if (id != null ? !id.equals(that.id) : that.id != null) {
            return false;
        }
        if (name != null ? !name.equals(that.name) : that.name != null) {
            return false;
        }
        if (!Objects.equals(amount, that.amount)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (amount != null ? amount.hashCode() : 0);
        return result;
    }
}
